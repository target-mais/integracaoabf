﻿using System;
using Target.Marketing.IntegracaoABF.Service.Executor;

namespace Target.Marketing.IntegracaoA
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Aperte qualquer tecla para iniciar");
            while (Console.ReadLine() != "target2020")
            {
                Console.WriteLine("O serviço foi iniciado");
                new LeadExecutor().IniciarProcessamento();
                
            }
            Console.WriteLine("O serviço foi interrompido ");
            Environment.Exit(1);
           
        }
    }
}

﻿using Refit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Target.Marketing.IntegracaoABF.Domain.Entities;

namespace Target.Marketing.IntegracaoABF.Domain.Interface
{
    public interface ILeadService
    {
        Task<Root> ConsultarLead();    
        Task<string> EnviarLead(List<Data> leads);


    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Target.Marketing.IntegracaoABF.Domain.Entities
{
    [DataContract(Name = "Data")]
    public class Data
    {
        [JsonProperty("Form")]
        public string Formulario { get; set; }

        [JsonProperty("Investment")]
        public string Investimento { get; set; }

        [JsonProperty("InvestmentForm")]
        public string FormularioInvestimento { get; set; }
        [JsonProperty("Name")]
        public string Nome { get; set; }
        [JsonProperty("Email")]
        public string Email { get; set; }
        [JsonProperty("Sex")]
        public string Sexo { get; set; }
        [JsonProperty("Phone")]
        public string Telefone { get; set; }

        [JsonProperty("Address")]
        public Address Endereco { get; set; }

        [JsonProperty("CitiesOfInterest")]
        public string CidadeInteresse { get; set; }

        [JsonProperty("CityThatWhantsOpen")]
        public string CidadeAtuar { get; set; }

        [JsonProperty("FranchisingTypes")]
        public string TipoFranquia { get; set; }

        [JsonProperty("Comment")]
        public string Comentario { get; set; }
        [JsonProperty("Created")]
        public DateTime Criacao { get; set; }

        [JsonProperty("Survey")]
        public Survey Pesquisa { get; set; }


    }
    public class Root
    {
        [JsonProperty("Data")]
        public List<Data> Data { get; set; }
        public string retorno { get; set; }
    }
}

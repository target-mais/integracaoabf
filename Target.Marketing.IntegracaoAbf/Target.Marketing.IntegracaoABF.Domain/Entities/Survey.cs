﻿using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace Target.Marketing.IntegracaoABF.Domain.Entities
{
    [DataContract(Name = "Survey")]
    public class Survey
    {
        [JsonProperty("Month")]
        public string Month { get; set; }
        [JsonProperty("AcademicEducation")]
        public string AcademicEducation { get; set; }
        [JsonProperty("Situation")]
        public string Situation { get; set; }
        [JsonProperty("Cpf")]
        public string Cpf { get; set; }
        [JsonProperty("Finished")]
        public bool Finished { get; set; }

    }
}
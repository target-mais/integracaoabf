﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Target.Marketing.IntegracaoABF.Domain.Entities
{
    [DataContract(Name = "Address")]
    public class Address
    {
        [JsonProperty("Name")]
        public string Name { get; set; }
        [JsonProperty("ZipCode")]
        public string ZipCode { get; set; }
        [JsonProperty("Number")]
        public string Number { get; set; }
        [JsonProperty("Complement")]
        public string Complement { get; set; }
        [JsonProperty("District")]
        public string District { get; set; }
        [JsonProperty("City")]
        public string City { get; set; }
        [JsonProperty("State")]
        public State State { get; set; }
        [JsonProperty("Country")]
        public Country Country { get; set; }
    }

    [DataContract(Name = "State")]
    public class State
    {
        [JsonProperty("Name")]
        public string Name { get; set; }
        [JsonProperty("Prefix")]
        public string Prefix { get; set; }
    }

    [DataContract(Name = "Country")]
    public class Country
    {
        [JsonProperty("Name")]
        public string Name { get; set; }
    }
}

﻿using AutoMapper;
using Refit;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Target.Marketing.IntegracaoABF.Domain.Entities;
using Target.Marketing.IntegracaoABF.Domain.Interface;
using Microsoft.Extensions.Configuration;
using Target.Marketing.IntegracaoABF.Service.Utils;
using System.Linq;
using Newtonsoft.Json;
using System.Net.Http;
using RestSharp;

namespace Target.Marketing.IntegracaoABF.Service
{
    public class LeadService : ILeadService
    {
        HttpClient client;
        private readonly IConfiguration _configuration;

        public LeadService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<Root> ConsultarLead()
        {
           Root leads = new Root();
           var apiABF =  _configuration.GetSection("PortalABF").Value;

            //evitar erro de certificação "SSL" colocar antes de chamadas httpclient
            var httpClienteHandler = new HttpClientHandler { };
            httpClienteHandler.ClientCertificateOptions = ClientCertificateOption.Manual;
            httpClienteHandler.ServerCertificateCustomValidationCallback = (HttpRequestMessage, cert, cetChain, policyErrors) => { return true; };

            //client = new HttpClient(httpClienteHandler);
            //client.BaseAddress = new Uri(apiABF);
            //client.DefaultRequestHeaders.Accept.Clear();
            //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("aplication/json"));
            //client.DefaultRequestHeaders.Add("accept-language", "pt-Br");
            //client.DefaultRequestHeaders.Add("apiKey", "6F053F99-DAB9-4F4A-B893-F4E9701F6844");
            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("apiKey", "6F053F99-DAB9-4F4A-B893-F4E9701F6844");
            //var alo = client.GetStringAsync("products/catafranquias");
            
            Console.WriteLine("consultando site da Abf - " + DateTime.Now);
            var cliente = new RestClient(apiABF);
            cliente.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("apiKey", Acesso.APIKEYABF);
            //request.AddHeader("apiKey", "6F053F99-DAB9-4F4A-B893-F4E9701F6844 ");
            IRestResponse response = cliente.Execute(request);
            var retorno  = response.Content;
            if (response.IsSuccessful && retorno != null)
            {               
                leads = JsonConvert.DeserializeObject<Root>(retorno);
                Console.WriteLine("A consulta retornou " + leads.Data.Count + " leads - " + DateTime.Now);
            }
            else
            {
                leads.retorno = retorno;
                Console.WriteLine(leads.retorno);
            }
           
            return leads;
        }

        public async Task<string> EnviarLead(List<Data> leads)
        {
            var msgRetorno = "";
            if (leads.Any())
            {
                Console.WriteLine("Enviando novos leads para jotForm");
                foreach (var lead in leads)
                {
                    var client = new RestClient("https://submit.jotformz.com/submit/202236778097666/?" +
                        "q1_nome=" + lead.Nome + "&" +
                        "q2_email=" + lead.Email + "&" +
                        "q3_celulartelefone=" + lead.Telefone + "&" +
                        "q4_cidade=" + lead.Endereco.City + "&" +
                        "q20_qualSeu=" + lead.Endereco.State.Name + "&" +
                        "q13_quandoabrir=&" +
                        "q10_invetimento=" + lead.Investimento + "&" +
                        "q19_cidadeDe=" + lead.CidadeInteresse);
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Cookie", "theme=tile-black; guest=guest_8f4fdd425c097917; last_form=202236778097666");
                    IRestResponse response = client.Execute(request);
                    int qtdLeads = 1;
                    Console.WriteLine("Enviando " + qtdLeads.ToString() + " - " + lead.Nome + " " + response.Content);
                }
                msgRetorno = "Leads enviados com Sucesso ";
                Console.WriteLine(msgRetorno + DateTime.Now);
            }
            else
            {
                msgRetorno = "E Necessário passar uma lista de leads para integrar ao pipeDrive ";
                Console.WriteLine(msgRetorno + DateTime.Now);
            }

            return msgRetorno;
        }
    }
}

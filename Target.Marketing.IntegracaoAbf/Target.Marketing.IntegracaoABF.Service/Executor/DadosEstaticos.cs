﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Target.Marketing.IntegracaoABF.Domain.Entities;

namespace Target.Marketing.IntegracaoABF.Api.V1.Executor
{
    public static class DadosEstaticos
    {
        public static Root LeadAnterior { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using Target.Marketing.IntegracaoABF.Domain.Entities;
using Target.Marketing.IntegracaoABF.Service.Commom;
using Target.Marketing.IntegracaoABF.Api.V1.Executor;

namespace Target.Marketing.IntegracaoABF.Service.Executor
{
    public class LeadExecutor : BaseExecutor
    {
        public LeadExecutor()
        {
            NomeTimer = "ConsultarLeads";

        }
        protected override async void Processar(object sender, ElapsedEventArgs e)
        {

            //TimerExecutor.Enabled = false;
            Root root = await leadService.ConsultarLead();
            if (DadosEstaticos.LeadAnterior == null && root.Data != null)
            {
                DadosEstaticos.LeadAnterior = root;
                await leadService.EnviarLead(DadosEstaticos.LeadAnterior.Data);
            }
            else if (root.Data != null)
            {        
                List<Data> novosleads = DadosEstaticos.LeadAnterior.Data.Intersect(root.Data).ToList();

                if (novosleads.Any())
                {
                    Console.WriteLine("Enviando " + DadosEstaticos.LeadAnterior.Data.Count + " novos leads para jotForm - " + DateTime.Now);
                    await leadService.EnviarLead(DadosEstaticos.LeadAnterior.Data);
                }
                else
                {
                    Console.WriteLine("A consulta retornou os mesmos leads da consulta anterior - " + DateTime.Now);
                }


                //.Union(leadsConsultaAnterior.Except(root.Data))

            }
            List<Data> NovosLeads = new List<Data>();
            NovosLeads = root.Data;


            //TimerExecutor.Enabled = true;



        }
    }
    }

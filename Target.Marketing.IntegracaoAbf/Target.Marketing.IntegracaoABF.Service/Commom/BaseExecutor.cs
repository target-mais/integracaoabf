﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Timers;

namespace Target.Marketing.IntegracaoABF.Service.Commom
{
    public abstract class BaseExecutor
    {
        protected Timer TimerExecutor { get; set; }

        protected string NomeTimer { get; set; }

        protected LeadService leadService { get; set; }


        public BaseExecutor()
        {
            leadService = new LeadService(configuration);
        }


        public static IConfigurationRoot configuration
        {
            get
            {
                return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            }
        }
        public virtual void IniciarProcessamento()
        {
            TimerExecutor = new Timer(Convert.ToDouble(900000));
            TimerExecutor.Elapsed += Processar;
            TimerExecutor.Start();
        }

        protected abstract void Processar(object sender, ElapsedEventArgs e);

    }
}
